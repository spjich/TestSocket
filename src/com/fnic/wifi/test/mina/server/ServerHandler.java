package com.fnic.wifi.test.mina.server;

import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import com.fnic.wifi.test.mina.SendMsg;

public class ServerHandler implements IoHandler
{

    @Override
    public void exceptionCaught(IoSession arg0, Throwable arg1) throws Exception
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void messageReceived(IoSession arg0, Object arg1) throws Exception
    {
        System.out.println("服务器收到：" + arg1);
        SendMsg s = new SendMsg(arg0, "111");
        s.start();
        // deliverRawText("1111", arg0);
    }

    @Override
    public void messageSent(IoSession arg0, Object arg1) throws Exception
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void sessionClosed(IoSession arg0) throws Exception
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void sessionCreated(IoSession arg0) throws Exception
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void sessionIdle(IoSession arg0, IdleStatus arg1) throws Exception
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void sessionOpened(IoSession arg0) throws Exception
    {
    }
}
