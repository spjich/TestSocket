package com.fnic.wifi.test.mina;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.mina.core.session.IoSession;

public class SendMsg extends Thread
{

    private static AtomicInteger ai = new AtomicInteger();

    public String a;

    IoSession session;

    public SendMsg(IoSession arg0, String a)
    {
        this.a = a;
        session = arg0;
    }

    @Override
    public void run()
    {
        // int i = 0;
        while (true)
        {
            int c = ai.incrementAndGet();
            session.write(c);
            System.out.println(Thread.currentThread().getId() + "发送了" + c);
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
