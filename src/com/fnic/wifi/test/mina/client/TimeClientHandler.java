package com.fnic.wifi.test.mina.client;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

public class TimeClientHandler extends IoHandlerAdapter {

    public void messageReceived(IoSession session, Object message) throws Exception {
        System.out.println("mina客户端收到 : " + message);
    }

    public void messageSent(IoSession session, Object message) throws Exception {
        System.out.println("mina客户端发送 " + message);
    }

}