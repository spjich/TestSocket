package com.fnic.wifi.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class SocketClient {  
    public static void main(String[] args) throws IOException
    {
        Socket s = new Socket("localhost", 5222);
        DataOutputStream out = new DataOutputStream(s.getOutputStream());
        out.write("中文\n".getBytes());
        // DataInputStream in = new DataInputStream(s.getInputStream());
        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        String str = in.readLine();
        while (!str.equals("quit"))
        {
            System.out.println("socket客户端收到:" + str);
            str = in.readLine();

        }
        out.close();
    }
}  